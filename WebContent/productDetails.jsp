<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<title>Insert title here</title>
</head>
<body>
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src = "https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
      
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src = "//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

	<form method="POST" action="displaySelectedItems">
	<div class="text-danger" align="center"><h4>Check the items that you want to include in this shipment.</h4></div>
	<table border="1" align="center" class = "table" style="width:80%">
		<tr align="center" class = "success">
			<td>&nbsp;</td>
			<td><b>UPC</b></td>
			<td><b>Description</b></td>
			<td><b>Price</b></td>
			<td><b>Weight</b></td>
			<td><b>Shipment Method</b></td>
			<td><b>Quantity</b></td>
		</tr>
		<c:forEach items="${sessionScope.productDetails}" var="productDtls">
		  <tr class = "warning">
		  	<td><input type="checkbox" name="productsChecked" value="${productDtls.upc}@${productDtls.description}@${productDtls.price}@${productDtls.weight}@${productDtls.shippingMethod}@${productDtls.shippingCost}"></td>
		     <td>${productDtls.upc}</td>
		     <td>${productDtls.description}</td>
		     <td>${productDtls.price}</td>
		     <td>${productDtls.weight}</td>
		     <td>${productDtls.shippingMethod}</td>
		     <td><input type="text" name="quantity${productDtls.upc}" maxlength="1"></td>
		  </tr>
		</c:forEach>
		</table>
		<br/>
		<div align="center"><button type="submit" class="btn btn-danger">Create Shipment</button></div>
	</form>
</body>
</html>