import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;




public class Product implements Serializable,Comparable<Product>{
	
	private static final long serialVersionUID = 1L;
	
	private static List<Product> productsList= new LinkedList<Product>();//code on interface
	
	private String upc;
	private String description;
	private double price;
	private double weight;
	private String shippingMethod;
	private double shippingCost;
	private int quantity;
	
	public Product(String upc, String desc, double price, double weight, String shippingMethod, double shippingCost,int quantity) {
		this.upc=upc;
		this.description=desc;
		this.price=price;
		this.weight=weight;
		this.shippingMethod=shippingMethod;
		this.shippingCost=shippingCost;
		this.quantity=quantity;
	}
	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	//constructor to initialize the items 
	public Product(String upc, String description, double price, double weight, String shippingMethod) {
		this.upc=upc;
		this.description=description;
		this.price=price;
		this.weight=weight;
		this.shippingMethod=shippingMethod;
		
	}
	
	public Product() {
		
	}

	//Add items in the list
	public void addProduct(Product product) {
		this.productsList.add(product);
		this.setStrategy(product);
	}
	
	//set the strategy and calculate shipping cost
	public void setStrategy(Product product) {
		String shippingMethod =product.getShippingMethod();
		if(shippingMethod.equals("AIR")) {
			double itemWeight=product.getWeight();
			String upcCodeStr=product.getUpc();
			
			//calculates the shippingTariff of cost of item
			double shippingTeriffOfItem=(product.getPrice()* 0.03);
			
			//get the second last digit of UPC code 
			String tempStringwithLastTwoChar=upcCodeStr.substring(upcCodeStr.length() - 2);
			String finalString=tempStringwithLastTwoChar.substring(0, 1);
			
			//Calculate the shipping cost and add shippingTariff of item 
			double shippingCost=itemWeight * Integer.parseInt(finalString);
			//not used
			//shippingCost+=shippingTeriffOfItem;
			
			DecimalFormat df = new DecimalFormat("#.##");      
			shippingCost = Double.valueOf(df.format(shippingCost));
			
			product.setShippingCost(shippingCost);
		}
		else if(shippingMethod.equals("GROUND")){
			double itemWeight=product.getWeight();
			
			double shippingCost=(itemWeight * 2.5);
			
			DecimalFormat df = new DecimalFormat("#.##");      
			shippingCost = Double.valueOf(df.format(shippingCost));
			
			product.setShippingCost(shippingCost);
		}
	}
	
	public List<Product> getAllProducts(){
		return productsList;
	}
	/**
	 * @return the upc
	 */
	public String getUpc() {
		return upc;
	}
	/**
	 * @param upc the upc to set
	 */
	public void setUpc(String upc) {
		this.upc = upc;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	/**
	 * @return the weight
	 */
	public double getWeight() {
		return weight;
	}
	/**
	 * @param weight the weight to set
	 */
	public void setWeight(double weight) {
		this.weight = weight;
	}
	/**
	 * @return the shippingMethod
	 */
	public String getShippingMethod() {
		return shippingMethod;
	}
	/**
	 * @param shippingMethod the shippingMethod to set
	 */
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	/**
	 * @return the shippingCost
	 */
	public double getShippingCost() {
		return shippingCost;
	}
	/**
	 * @param shippingCost the shippingCost to set
	 */
	public void setShippingCost(double shippingCost) {
		this.shippingCost = shippingCost;
	}

	public int compareTo(Product productObj) {
		//ascending order
		return this.shippingMethod.compareTo(productObj.getShippingMethod());
	}
}
