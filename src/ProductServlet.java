import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 
 */

/**
 * @author gyelwand
 *
 */
public class ProductServlet extends HttpServlet{
	
/**
	 * 
	 */
	private static final long serialVersionUID = -1530307542137643931L;
	
	private final static Logger LOGGER = Logger.getLogger(ProductServlet.class.getName()); 

/**
	 * 
	 */
	public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		
		//it is used to check whether the display page is welcome page or products details diplay page
		String action = request.getParameter("action");
		LOGGER.info("action is-->"+action);
		
		//if it is welcome page clear the session/request and forward the request to index page
		if(action!=null && action.equalsIgnoreCase("welcomePage")) {
			HttpSession session = request.getSession(false);
			if(session!=null) {
				session.invalidate();
			}
			
			String forwardPage="Index.html";
		    request.getRequestDispatcher(forwardPage).forward(request, response);
			
		}
		else {
			LOGGER.info("Servicing request for the servlet in doGet method!!!");
			
			Product productObj= new Product();
			
			HttpSession session = request.getSession();
			//retrieve all the products and store it in session scope to display on page
			session.setAttribute("productDetails", productObj.getAllProducts());
			LOGGER.info("productObj.getAllProducts() size in doGet is-->"+productObj.getAllProducts().size());
			
			String forwardPage="productDetails.jsp";
		    request.getRequestDispatcher(forwardPage).forward(request, response);
		}
		
	}
		
	@Override
	 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		  LOGGER.info("in doPost method*************");
		  
		  //Get the selected products on array of Strings
		  String[] productsSelected = request.getParameterValues("productsChecked");
		  int i=0;
		  String[] updatedProductList=new String[productsSelected.length];
		  double totalShippingCost=0.0;
		
		  PrintWriter pw = response.getWriter();
		  
		  //Create file object to write the selected products
		  File file = new File("C:\\Users\\gyelwand\\ShippedProductsLog.txt");
		  FileWriter fstream = new FileWriter(file,true);
		  BufferedWriter out = new BufferedWriter(fstream);
		  
		  List<Product> productDetailsTemp=new ArrayList<Product>();
		  String[] productUPC;
		  
		  //iterate through each product to find the matching UPC
		  for(String str:productsSelected) {
			  productUPC = str.split("@");
			  LOGGER.info("productUPC[0] is-->"+productUPC[0]);
			  String quantity = request.getParameter("quantity" + productUPC[0]);
			  
			  int quantityInt= Integer.parseInt(quantity);
			
			  //create the product object
			  Product productTemp =new Product(productUPC[0], productUPC[1], Double.parseDouble(productUPC[2]), Double.parseDouble(productUPC[3]), 
					  productUPC[4], Double.parseDouble(productUPC[5]), quantityInt);
			  
			  //store the selected product in list
			  productDetailsTemp.add(productTemp);
		    }
		   
		  
		    // Sort all the selected products based on shipping method
		    Collections.sort(productDetailsTemp);
			 
			for(Product productItemTemp:productDetailsTemp) {
		       LOGGER.info("after sorting shipping method is-->"+productItemTemp.getShippingMethod());
			   productItemTemp.setShippingCost(productItemTemp.getShippingCost()*productItemTemp.getQuantity());
			  
			   totalShippingCost+=productItemTemp.getShippingCost();
			  
			   DecimalFormat df = new DecimalFormat("#.##");      
			   totalShippingCost = Double.valueOf(df.format(totalShippingCost));
			  
			   updatedProductList[i]=productItemTemp.getUpc()+"\t"+productItemTemp.getDescription()+"\t"+productItemTemp.getPrice()+"\t"+
					  productItemTemp.getWeight()+"\t"+productItemTemp.getQuantity()+"\t"+productItemTemp.getShippingMethod()
					  +"\t"+productItemTemp.getShippingCost();
			 
			  
			   out.write(updatedProductList[i]);
			   out.newLine();
			   i++;
			}
			  
			out.close();
			pw.println("File is created successfully");
			  
		    HttpSession session = request.getSession(true);
		    List<String> selectedProducts =  Arrays.asList(updatedProductList); 
		  
		    session.setAttribute("totalShippingCost", totalShippingCost); 
		  
		    session.setAttribute("selectedProducts", selectedProducts); 
		  
		    String forwardPage="productDetailsReport.jsp";
		  request.getRequestDispatcher(forwardPage).forward(request, response);
		
	 }

	
	public void init(ServletConfig servletConfig) throws ServletException {
		
		LOGGER.info("Servlet Initialization started!!!");
		
		Product productObject1= new Product("567321101987","CD - Pink Floyd, Dark Side Of The Moo",19.99,0.58, "AIR");
		Product productObject2= new Product("567321101986","CD - Beatles, Abbey Road",17.99,0.61, "GROUND");
		Product productObject3= new Product("567321101985","CD - CD - Queen, A Night at the Opera",20.49,0.55, "AIR");
		Product productObject4= new Product("567321101984","CD - Michael Jackson, Thriller",23.88,0.50, "GROUND");
		Product productObject5= new Product("467321101899","iPhone - Waterproof Case",9.75,0.73, "AIR");
		Product productObject6= new Product("477321101878","iPhone -  Headphones",17.25,3.21, "GROUND");
		
		Product productObj=new Product();
		
		productObj.addProduct(productObject1);
		productObj.addProduct(productObject2);
		productObj.addProduct(productObject3);
		productObj.addProduct(productObject4);
		productObj.addProduct(productObject5);
		productObj.addProduct(productObject6);
		
		LOGGER.info("Products list size is-->"+productObj.getAllProducts().size());
		
	}

	public void destroy() {
		LOGGER.info("Servlet is removed out of the Container!!!");
	}

}
